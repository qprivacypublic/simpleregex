// 3F2504E0-4F89-11D3-9A0C-0305E82C3301

const findMode = ["(?<M1>:i)", "(?<M2>:w)"];

const escapeRegex = (e: string) =>
  e.replace(/([\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$1");

const textFind = (code: string, text: string) =>
  `(?<${code}>${escapeRegex(text)})`;

const DIGIT = "\\d";
const DIGIT_RANGE = "\\d+\\-\\d+";
const CHAR = "[ux]\\d+";
const CHAR_RANGE = `${CHAR}\\-${CHAR}`;

const CHAR_GROUP = ["(", CHAR, "|", CHAR_RANGE, ")"].join("");
const CHAR_GROUP_ARRAY = [CHAR_GROUP, "(," + CHAR_GROUP + ")*"].join("");
const CHAR_GROUP_NAME = `(${CHAR_GROUP_ARRAY})`;

const groupBuilder = (code: string, not: boolean, action: string) => {
  return `(?<${code}>\\{${not ? "!" : ""}\\[${CHAR_GROUP_NAME}\\]${action}\\})`;
};

const findPart = [
  textFind("C0", "*"),
  textFind("C1", "?"),
  textFind("C2", "#"),

  textFind("G1", "{##}"),
  textFind("G1", "{domain}"),
  textFind("G2", "{guid}"),
  textFind("G3", "{hash}"),
  textFind("G4", "{ascii}"), // https://stackoverflow.com/a/46413244/1997873
  textFind("G4", "{~ascii}"), // https://stackoverflow.com/a/46413244/1997873

  groupBuilder("R0", false, ""),
  groupBuilder("R1", false, "\\*"),
  groupBuilder("R2", false, "\\+"),
  groupBuilder("R3", false, `\\[${DIGIT_RANGE}\\]`),

  groupBuilder("RN0", true, ""),
  groupBuilder("RN1", true, "\\*"),
  groupBuilder("RN2", true, "\\+"),
  groupBuilder("RN3", true, `\\[${DIGIT_RANGE}\\]`),
];

const findPartRGX = new RegExp(findPart.join("|"), "g");

console.log(findPartRGX);

const example = "/ep/{##}/cam??/{[u5000-u7000,x10-x15][4-5]}";

for (let i = 0; i < example.length; i++) {
  const result = findPartRGX.exec(example);
  if (!result) {
    console.log("Done looping! " + i);
    break;
  } else {
    console.log(result.index, result.groups);
  }
}
findPartRGX.lastIndex = 0; // reset regex
